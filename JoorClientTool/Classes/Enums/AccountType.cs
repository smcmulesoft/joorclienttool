﻿namespace JoorClientTool.Classes.Enums
{
    public enum AccountType
    {
        Womens = 1,
        Mens = 2, 
        Kids = 3
    }
}
