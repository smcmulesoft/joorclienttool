﻿using JoorClientTool.Data;
using JoorClientTool.Models;
using JoorClientTool.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoredProcedureEFCore;
using System.Collections.Generic;
using System.Linq;

namespace JoorClientTool.Controllers
{
    public class ClientController : Controller
    {
        private readonly JoorDatabaseContext _context;

        public ClientController(JoorDatabaseContext context)
        {
            _context = context;
        }

        // GET: ClientControler
        [HttpGet]
        public ActionResult Index(string id, string text)
        {
            var model = new DimJoorClientsViewModel();
            if (!string.IsNullOrEmpty(text)) model.Message = text;
            model.SearchString = id;
            model.Clients = string.IsNullOrEmpty(id)
                ? _context.Clients.OrderByDescending(x => x.CustomerCode).Take(5).ToList()
                : _context.Clients.Where(x => x.CustomerCode.Contains(id)).OrderBy(x => x.CustomerCode).Take(5).ToList();
            model.Buyers = string.IsNullOrEmpty(id)
                ? _context.Buyers.OrderByDescending(x => x.CustomerCode).Take(5).ToList()
                : _context.Buyers.Where(x => x.CustomerCode.Contains(id)).OrderBy(x => x.CustomerCode).Take(5).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(string searchString, int dumb = 0)
        {
            if (searchString != null)
            {
                searchString = searchString.Trim();
            }
            var model = new DimJoorClientsViewModel();
            model.SearchString = searchString;
            model.Clients = string.IsNullOrEmpty(searchString)
                ? _context.Clients.OrderByDescending(x => x.CustomerCode).Take(5).ToList()
                : _context.Clients.Where(x => x.CustomerCode.Contains(searchString)).OrderBy(x => x.CustomerCode).Take(5).ToList();
            model.Buyers = string.IsNullOrEmpty(searchString)
                ? _context.Buyers.OrderByDescending(x => x.CustomerCode).Take(5).ToList()
                : _context.Buyers.Where(x => x.CustomerCode.Contains(searchString)).OrderBy(x => x.CustomerCode).Take(5).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult GetJobComplete()
        {
            var rows = new List<RuningUpdateResult>();
            _context.LoadStoredProc("spIsUpdateJobRunning").Exec(r => rows = r.ToList<RuningUpdateResult>());
            var isComplete = rows.Count > 0;
            var result = Json(isComplete);
            return result;
        }

        public ActionResult RunUpdateJob()
        {
            var rows = new List<RuningUpdateResult>();
            _context.LoadStoredProc("spIsUpdateJobRunning").Exec(r => rows = r.ToList<RuningUpdateResult>());
            var isRunning = rows.Count < 1;

            if(isRunning)
            {
                return RedirectToAction("Index", new { @text = "Job already running"});
            }
            _context.LoadStoredProc("spRunUpdateJob").ExecNonQuery();
            return View();
        }


        // GET: ClientControler/Create
        public ActionResult Create()
        {
            var model = new DimJoorClientViewModel();
            model.Client = new DimJoorClient();
            model.AllAccountTypes = GetAccountTypes();
            return View(model);
        }

        private IEnumerable<SelectListItem> GetAccountTypes(int? selectedType = 0)
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() { Value = "1", Text = "Womens", Selected = selectedType == 1 },
                new SelectListItem() { Value = "2", Text = "Mens", Selected = selectedType == 2 },
                new SelectListItem() { Value = "3", Text = "Kids", Selected = selectedType == 3 }
            };
            return result;
        }

        // POST: ClientControler/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("ErrorMessage,Client")] DimJoorClientViewModel clientvm)
        {
            clientvm.AllAccountTypes = GetAccountTypes(clientvm.Client.Account);

            if (!ModelState.IsValid)
            {
                return View(clientvm);
            }

            var dbclient = _context.Clients.FirstOrDefault(x => x.CustomerCode == clientvm.Client.CustomerCode);
            if (dbclient != null)
            {
                clientvm.ErrorMessage = $"Client with Id {clientvm.Client.CustomerCode} already exists";
                ModelState.AddModelError(clientvm.ErrorMessage, clientvm.ErrorMessage);
                return View(clientvm);
            }

            var dbBuyer = _context.Buyers.FirstOrDefault(x => x.CustomerCode == clientvm.Client.CustomerCode);
            if (dbBuyer != null)
            {
                clientvm.ErrorMessage = $"Buyer with Id {clientvm.Client.CustomerCode} already exists";
                ModelState.AddModelError(clientvm.ErrorMessage, clientvm.ErrorMessage);
                return View(clientvm);
            }

            var buyer = new DimJoorBuyer();

            buyer.CustomerCode = clientvm.Client.CustomerCode;
            buyer.BuyerEmail = clientvm.Client.CustomerContactEmail;
            buyer.Account = clientvm.Client.Account;

            _context.Buyers.Add(buyer);
            _context.Clients.Add(clientvm.Client);
            _context.SaveChanges();

            return RedirectToAction("Index", "Client", new { id = clientvm.Client.CustomerCode });
        }

        // GET: ClientControler/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ClientControler/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ClientControler/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ClientControler/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
