﻿using JoorClientTool.Models;
using System.Collections.Generic;

namespace JoorClientTool.ViewModels
{

    public class DimJoorClientsViewModel
    {
        public string Message { get; set; }
        
        public string SearchString { get; set; }

        public string ErrorMessage { get; set; }

        public IEnumerable<DimJoorClient> Clients { get; set; }

        public IEnumerable<DimJoorBuyer> Buyers { get; set; }
    }
}
