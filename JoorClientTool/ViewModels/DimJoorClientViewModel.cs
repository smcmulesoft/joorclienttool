﻿using JoorClientTool.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace JoorClientTool.ViewModels
{
    public class DimJoorClientViewModel
    {
        public string ErrorMessage { get; set; }
        public DimJoorClient Client { get; set; }

        public IEnumerable<SelectListItem> AllAccountTypes;
    }
}
