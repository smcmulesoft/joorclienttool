﻿using JoorClientTool.Models;
using Microsoft.EntityFrameworkCore;

namespace JoorClientTool.Data
{
    public class JoorDatabaseContext : DbContext
    {
        public JoorDatabaseContext(DbContextOptions<JoorDatabaseContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<DimJoorBuyer> Buyers { get; set; }
        public DbSet<DimJoorClient> Clients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<DimJoorClient>().HasKey(x => new { x.CustomerCode, x.CustomerContactEmail });
            modelBuilder.Entity<DimJoorBuyer>().HasKey(x => new { x.CustomerCode, x.BuyerEmail });
        }

    }
}
