﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JoorClientTool.Models
{
    [Table("dimJoorBuyers")]
    public class DimJoorBuyer
    {
        [Column("customer_code")]
        public string CustomerCode { get; set; }

        [Column("buyer_email")]
        public string BuyerEmail { get; set; }

        [Column("buyer_name")]
        public string BuyerName { get; set; }

        [Column("account")]
        public int? Account { get; set; }
    }
}
