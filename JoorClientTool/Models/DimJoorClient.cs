﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JoorClientTool.Models
{
    [Table("dimJoorClient")]
    public class DimJoorClient
    {
        [Required]
        [Column("customer_code")]
        public string CustomerCode { get; set; }

        [Required]
        [Column("customer_contact_email")]
        public string CustomerContactEmail { get; set; }

        [Required]
        [Column("price_type_label")]
        public string PriceTypeLabel { get; set; }

        [Required]
        [Column("price_type_currency_code")]
        public string PriceTypeCurrencyCode { get; set; }

        [Required]
        [Column("sales_rep")]
        public string SalesRep { get; set; }

        [Required]
        [Column("account")]
        public int? Account { get; set; }
    }
}
