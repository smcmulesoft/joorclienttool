﻿$(document).ready(function () {
    var dots = 3;
    var display = document.querySelector('#PleaseWait');
    startTimer(display);

    function RedirectComplete() {
        window.location.href = "/Client/Index";
    }

    function startTimer(display) {
        setInterval(function () {

            if (dots == 3) {
                dots = 0;
                $.getJSON("/Client/GetJobComplete", function (result) {
                    if (result == true) RedirectComplete();
                });
            };

            dots = dots + 1;
            var content = 'Please Wait ';
            if (dots == 1) { content = content + '.'; }
            if (dots == 2) { content = content + '..'; }
            if (dots == 3) { content = content + '...'; }

            if (display != null) { display.textContent = content; }
        }, 1000);
    }
});